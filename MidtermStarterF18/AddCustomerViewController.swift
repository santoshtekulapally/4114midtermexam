//
//  AddCustomerViewController.swift
//  MidtermStarterF18
//
//  Created by parrot on 2018-11-14.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import CoreData

class AddCustomerViewController: UIViewController {

    var context:NSManagedObjectContext!

    var Random:String!
    // MARK: Outlets
    // ---------------------
    @IBOutlet weak var nameTextBox: UITextField!
    @IBOutlet weak var startingBalanceTextBox: UITextField!
    @IBOutlet weak var messageLabel: UILabel!
    
    // MARK: Default Functions
    // ---------------------
    override func viewDidLoad() {
        super.viewDidLoad()

      
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        // 2. Mandatory - initialize the context variable
        // This variable gives you access to the CoreData functions
        self.context = appDelegate.persistentContainer.viewContext

        // HINT HINT HINT HINT HINT
        // HINT HINT HINT HINT HINT
        // Code to create a random 4 digit string
        var x:String = ""
        repeat {
            // Create a string with a random number 0...9991
            x = String(format:"%04d", arc4random_uniform(9991) )
        } while x.count < 4
        
        print("Random value: \(x)")
        Random = x
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    // MARK: Actions
    // ---------------------
    
    @IBAction func createAccountPressed(_ sender: Any) {
        
        print("Random value is  \(Random)")

        print("you pressed the create account button!")
        
        
       // let m = Bank(context: self.context)
        
      //     m.customerid = Random
    //    m.name = nameTextBox.text!
     //   m.balance = startingBalanceTextBox.text!
        
        let  u = Users(context: self.context)
        u.name = nameTextBox.text!
        u.balance = startingBalanceTextBox.text!
        u.id = Random
        
        
        
        do {
            // Save the user to the database
            // (Send the INSERT to the database)
            try self.context.save()
            print("Saved to database!")
            messageLabel.text = "User saved to database"

        }
        catch {
            print("Error while saving to database")
            
            messageLabel.text = "Error while saving to database"
        }
        
        
    }
    
    
  
    
    
    @IBAction func showpressed(_ sender: Any) {
        
        
        print("Show all users pressed!")
        
        // This is the same as:  SELECT * FROM User
        
        //SELECT * FROM User
        let fetchRequest:NSFetchRequest<Users> = Users.fetchRequest()
        
        //WHERE email="jenelle@gmail.com"
        //fetchRequest.predicate =  NSPredicate(format: "email == %@", "jenelle@gmail.com")
        
        // SQL: SELECT * FROM User WHERE email="jeenlle@gmil.com"
        
        do {
            // Send the "SELECT *" to the database
            //  results = variable that stores any "rows" that come back from the db
            // Note: The database will send back an array of User objects
            // (this is why I explicilty cast results as [User]
            let results = try self.context.fetch(fetchRequest) as [Users]
            
            // Loop through the database results and output each "row" to the screen
            print("Number of items in database: \(results.count)")
            
            for x in results {
                print("User id: \(x.id)")
                print("User balance: \(x.balance)")
                print("User name: \(x.name)")

            }
        }
        catch {
            print("Error when fetching from database")
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        print("STEP 1: HELLO!!!!")
        
        //let screen2 = segue.destination as! SearchResultsViewController
        //screen2.personName = self.searchTextBox.text!
        
        let deposit = segue.destination as! DepositViewController
        
        //SELECT * FROM User WHERE email = .....
        let fetchRequest:NSFetchRequest<Users> = Users.fetchRequest()
        fetchRequest.predicate =  NSPredicate(format: "name == %@", nameTextBox.text!)
        
        do {
            
            let results = try self.context.fetch(fetchRequest) as [Users]
            
            // Loop through the database results and output each "row" to the screen
            print("Number of items in database: \(results.count)")
            
            if (results.count == 1) {
                deposit.person = results[0] as Users
                print("results are \(results )")
                
                print("is all what ewe want: \(deposit.person )")
                
            }
            
        }
        catch {
            print("Error when fetching from database")
        }
        
        
    }
    
    
    
}
