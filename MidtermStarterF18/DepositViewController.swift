//
//  DepositViewController.swift
//  MidtermStarterF18
//
//  Created by parrot on 2018-11-14.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import CoreData


class DepositViewController: UIViewController {
    //
    var person:Users! // passing objects
    
    var context:NSManagedObjectContext!

    // MARK: Outlets
    // ---------------------
    @IBOutlet weak var customerIdTextBox: UITextField!
    @IBOutlet weak var balanceLabel: UILabel!

    @IBOutlet weak var depositAmountTextBox: UITextField!
    @IBOutlet weak var messagesLabel: UILabel!
    
    // MARK: Default Functions
    // ---------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        print("You are on the Check Balance screen!")
        
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        self.context = appDelegate.persistentContainer.viewContext
        
        
        // DEBUG: Print contents from database
      //  print("DEBUG: Email: \(self.person.email)")
       // print("DEBUG: id: \(self.person.id)")
      //  print("DEBUG: balance: \(self.person.balance)")

        
        //  UI update
     //   customerIdTextBox.text = self.person.id!
      //  passwordTextBox.text = self.person.password!
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

    // MARK: Actions
    // ---------------------
    
    
    @IBAction func checkBalancePressed(_ sender: Any) {
        print("check balance button pressed!")
        let fetchRequest:NSFetchRequest<Users> = Users.fetchRequest()
        
        //WHERE email="jenelle@gmail.com"
        fetchRequest.predicate =  NSPredicate(format: "id == %@", customerIdTextBox.text!)
        
        // SQL: SELECT * FROM User WHERE email="jeenlle@gmil.com"
        
        do {
            // Send the "SELECT *" to the database
            //  results = variable that stores any "rows" that come back from the db
            // Note: The database will send back an array of User objects
            // (this is why I explicilty cast results as [User]
            let results = try self.context.fetch(fetchRequest) as [Users]
            
            // Loop through the database results and output each "row" to the screen
            print("Number of items in database: \(results.count)")
            
            for x in results {
                print("User Email: \(x.balance)")
                print("User Password: \(x.name)")
                balanceLabel.text = x.balance
                
                
            }
        }
        catch {
            print("Error when fetching from database")
        }
    }
    
    
    @IBAction func depositButtonPressed(_ sender: Any) {
        
        
        
        
        let fetchRequest:NSFetchRequest<Users> = Users.fetchRequest()
        
        //WHERE email="jenelle@gmail.com"
        fetchRequest.predicate =  NSPredicate(format: "id == %@", customerIdTextBox.text!)
        
        // SQL: SELECT * FROM User WHERE email="jeenlle@gmil.com"
        
        do {
            // Send the "SELECT *" to the database
            //  results = variable that stores any "rows" that come back from the db
            // Note: The database will send back an array of User objects
            // (this is why I explicilty cast results as [User]
            let results = try self.context.fetch(fetchRequest) as [Users]
            
            // Loop through the database results and output each "row" to the screen
            print("Number of items in database: \(results.count)")
            
            for x in results {
                print("User Email: \(x.balance)")
                print("User Password: \(x.name)")
                balanceLabel.text = x.balance
                
                var bal:Int?=Int(x.balance!)
               var deposit:Int? = Int(depositAmountTextBox.text!)
                
                var new = (bal! + deposit!)
                
                messagesLabel.text = "\(new)"
                x.balance = "\(new)"
                
                do{
                try self.context.save()
                    
                    balanceLabel.text = "\(new)"

                }

                catch {
                    print("Error when fetching from database")
                }
                

                
            }
        }
        catch {
            print("Error when fetching from database")
        }
        
    }
    
    
    @IBAction func showCustomersPressed(_ sender: Any) {
        
        // This is the same as:  SELECT * FROM User
        
        //SELECT * FROM User
        let fetchRequest:NSFetchRequest<Users> = Users.fetchRequest()
        
        //WHERE email="jenelle@gmail.com"
        //fetchRequest.predicate =  NSPredicate(format: "email == %@", "jenelle@gmail.com")
        
        // SQL: SELECT * FROM User WHERE email="jeenlle@gmil.com"
        
        do {
            // Send the "SELECT *" to the database
            //  results = variable that stores any "rows" that come back from the db
            // Note: The database will send back an array of User objects
            // (this is why I explicilty cast results as [User]
            let results = try self.context.fetch(fetchRequest) as [Users]
            
            // Loop through the database results and output each "row" to the screen
            print("Number of items in database: \(results.count)")
            
            for x in results {
                print("User id: \(x.id)")
                print("User balance: \(x.balance)")
                print("User name: \(x.name)")
                
            }
        }
        catch {
            print("Error when fetching from database")
        }
        
    }

}
